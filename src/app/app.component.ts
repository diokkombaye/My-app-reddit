import { Article } from './components/article/article.module';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  articles : Article[]; 
  constructor(){
   this.articles = [
     new Article('Angular 2', 'http://angular.toto', 2)
   ];
  }

  addArticle(title : HTMLInputElement, link: HTMLInputElement): boolean{
    console.log('i click'),
    console.log(`Adding article title: ${title.value} and link ${link.value}`);
    this.articles.push(new Article(title.value, link.value, 0));
    title.value = ''; 
    link.value = ''; 
    return false; 
  }
}
