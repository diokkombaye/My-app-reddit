export class Article{
    title : string ; 
    link : string ; 
    votes : number ;

    constructor(title: string, link : string,votes: number){
        this.votes = votes || 0; // votes must be init 
        this.link = link; 
        this.title = title;
    }

    getTitle(){
        return this.title ; 
    }

    getVotes(){
    return this.votes; 
    }

    getlink(){
        return this.link;
    }
    voteUp() : void {
        this.votes += 1;
      }
    
      voteDown() : void {
        this.votes -= 1;
      }
      domain(): string{
        try {
            const link : string = this.link.split('//')[1];
            return link.split('/')[0]; 
        } catch (err) {
            return  null ;
        }
      }
}